<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class UserAdmin extends AbstractAdmin
{
    /*
     * These lines configure which fields are displayed on the edit and create actions.
     * The FormMapper behaves similar to the FormBuilder of the Symfony Form component
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('first_name', 'text')
            ->add('last_name', 'text')
            ->add('email', 'text')
            ->add('password', 'password')
            ->add('role', 'sonata_type_model', array(
                'class' => 'AppBundle\Entity\Role',
                'property' => 'name',
            ))
            ->add('team', 'sonata_type_model', array(
                'class' => 'AppBundle\Entity\Team',
                'property' => 'name',
                'multiple' => false,
            ))
        ;
    }

    /*
     * This method configures the filters, used to filter and sort the list of models
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('email')
        ;
    }

    /*
     * Here you specify which fields are shown when all models are listed (the addIdentifier()
     * method means that this field will link to the show/edit page of this particular model).
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('first_name')
            ->addIdentifier('last_name')
            ->addIdentifier('email')
            ->add('role.name')
        ;

    }
}
<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Role;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Collections\ArrayCollection;
use Faker\Factory as Faker;


class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 3; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $data = new Role();
        $em->persist($data); // Manage Entity for persistence.
        $data
            ->setName('Teamleider')
        ;
        $this->addReference('TestRole-1', $data); // Reference for the next Data Fixture(s).

        $data = new Role();
        $em->persist($data); // Manage Entity for persistence.
        $data
            ->setName('Teamlid')
        ;
        $this->addReference('TestRole-2', $data); // Reference for the next Data Fixture(s).


        $em->flush(); // Persist all managed Entities.
    }
}